import turtle 
import random
import time
screen = turtle.Screen()

painter = turtle.Turtle()
painter.speed(6000)
painter.pencolor("blue")
#turtle.tracer(0, 0)

for i in range(255):
    painter.forward(255)
    painter.left(123) # Let's go counterclockwise this time 

painter.penup ()  
painter.pencolor("red")
for i in range(100):
    painter.goto(127,70)
    painter.pendown ()
    painter.forward(50)
    painter.left(123)
   
turtle.done()
